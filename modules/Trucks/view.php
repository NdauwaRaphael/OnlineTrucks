<?php
@include('../../config/connect.php');
@include('../../config/session.php');
@include('../../config/query.php');
?>
<div class="row" id="sort">
  <h4>Search Truck</h4>
  <div class="col-lg-2">
    <select id="location" onchange="filterTrucks()" class="form-control input-sm">
      <option value="0">-----Location-----</option>
      <option value="Naivasha">Naivasha</option>
      <option value="Nakuru">Nakuru</option>
      <option value="Mombasa">Mombasa</option>
      <option value="Nairobi">Nairobi</option>
      <option value="Eldoret">Eldoret</option>
      <option value="Kiambu">Kiambu</option>
      <option value="Kisumu">Kisumu</option>
      
    </select>

  </div>
  <div class="col-lg-2">
    <select id="company" onchange="filterTrucks()" class="form-control input-sm">
      <option value="0">----Company-----</option>
          <?php 
                $result = mysqli_query($dbc, "SELECT * FROM `truck_company` ORDER BY `truck_company`.`Name` ASC");
                while ($nem = mysqli_fetch_array($result) ) {
                  $queriednem = $nem['Name'];
               ?>
                <option value="<?php echo "$queriednem"; ?>"><?php echo "$queriednem"; ?></option>
                <?php
                }
          ?>      
      <option value="Private owned">Private owned</option>
    </select>
  </div>
  <div class="col-lg-2">
    <select id="capacity" onchange="filterTrucks()" class="form-control input-sm">
      <option value="0">----Capacity-----</option>
      <option value="45tonnes">45tonnes</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>

  <div class="col-lg-2">
    <select id="category" onchange="filterTrucks()" class="form-control input-sm" center>
      <option value="0">-----Cargo category----</option>
<?php 
      $resul = mysqli_query($dbc, "SELECT * FROM `truck_categories` ORDER BY `truck_categories`.`category` ASC");
      while ($cat = mysqli_fetch_array($resul) ) {
        $queriedCategory = $cat['category'];
     ?>
      <option value="<?php echo "$queriedCategory"; ?>"><?php echo "$queriedCategory"; ?></option>
      <?php
      }
?>      

    </select>
  </div>

  <div class="col-lg-3">
    <select id="distance" class="form-control input-sm">
      <option value="0">-------Distance transported------</option>
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>


</div>

<div id="trucksDiv">



<?php

 /*=====================================
         Query Trucks Details
     ======================================*/
	$qt = "SELECT * FROM `trucks` LIMIT 8";
	if($rt = mysqli_query($dbc, $qt)){

	while($truck = mysqli_fetch_array($rt))
	{ 
  $tId = $truck['Id'];
	$tName = $truck['truckName'];
	$tNumber = $truck['truckNumber'];
	$company=$truck['company'];
	$area = $truck['areaOfOperation'];
	$capacity = $truck['cargoCapacity'];
	$category = $truck['cargoCategory'];
	$image = $truck['imagePath'];
  $owner = $truck['Owner'];


	?>

<!--List of available trucks-->
  <div class="col-sm-3 col-md-3">
    <div class="thumbnail">
      <img src="<?php if ($image=='empty') {
        echo "assets/images/noPic.jpg";
      }else{                
        echo $image;
        } ?>" alt="..." width="100%">
      <div class="caption">
        <h3><?php echo $tName. " ". $tNumber ?></h3>
        <p>A <?php echo $category; ?> Truck Operating in <strong><?php echo $area; ?>.  </strong><br>
         category - <?php echo $category; ?><br>
         Company - <?php echo $company; ?><br>
          </p>
        <p><button id="reserve<?php echo $tId; ?>" class="openTruckedBtn"><span class="glyphicon glyphicon-shopping-cart"></span> Reserve</button> </p>
      </div>
    </div>
  </div>
  <script type="text/javascript">

   $(document).ready(function(){
    $("#reserve<?php echo $tId; ?>").click(function(){
      var truckId = "<?php echo $tId; ?>";
      var truckName = "<?php echo $tName; ?>"; 
      var truckNumber = "<?php echo $tNumber; ?>"; 
      var truckCompany = "<?php echo $company; ?>";
      var truckArea = "<?php echo $area; ?>";
      var truckCapacity = "<?php echo $capacity; ?>" ;
      var truckCategory = "<?php echo $category; ?>" ;
      var truckImage = "<?php echo $image; ?>" ;
      var truckOwner = "<?php echo $owner; ?>";

      if (truckId=='' || truckName==''|| truckNumber==''||truckCompany==''||truckArea==''||truckCapacity==''||truckCategory==''||truckImage==''||truckOwner=='') {

      }else{
         if ($.post('modules/Trucks/one.php', {truckId1:truckId, truckName1:truckName,truckNumber1:truckNumber,truckCompany1:truckCompany,truckArea1:truckArea,truckCapacity1:truckCapacity,truckCategory1:truckCategory,truckImage1:truckImage,truckOwner1:truckOwner}, function(data){
                $('#trucksDiv').html(data);
          })) {
                   
         }
      }


      });/*reseve button ends here*/
})

  </script>
<!--Loop around here-->
	<?php
    }

	}else{
		echo "failed";
	}
    
    ?>

    </div><div id="dit"></div>


<script type="text/javascript">
  function filterTrucks(){
        var location = document.getElementById('location').value;
        var company = document.getElementById('company').value;
        var category = document.getElementById('category').value;
        var distance = document.getElementById('distance').value;
        var capacity = document.getElementById('capacity').value;

       $.post('config/trucks/filterTrucks.php', {location1:location,company1:company,category1:category,distance1:distance,capacity1:capacity}, function(data){
           $('#trucksDiv').html(data);
        });      
  }

</script>

</div>
    <div class="row" >
        <ul class="pagination col-md-12">
          <li><span id="#next"> &laquo;</span></li>
          <li><a href="#one">1</a></li>
          <li><a href="#two">2</a></li>
          <li><a href="#three">3</a></li>
          <li><a href="#four">4</a></li>
          <li><a href="#five">5</a></li>
          <li><a href="#prev">&raquo;</a></li>
        </ul>      
    </div>