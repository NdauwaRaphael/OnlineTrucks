<div class="row">
 <?php
@include('../../config/connect.php');
@include('../../config/session.php');
@include('../../config/query.php');

if ($_POST) {
      $truckId = $_POST['truckId1'];
      $truckName = $_POST['truckName1']; 
      $truckNumber = $_POST['truckNumber1'];  
      $truckCompany = $_POST['truckCompany1']; 
      $truckArea = $_POST['truckArea1']; 
      $truckCapacity = $_POST['truckCapacity1']; 
      $truckCategory = $_POST['truckCategory1']; 
      $truckImage = $_POST['truckImage1'];  
      $truckOwner = $_POST['truckOwner1'];


 /*=====================================
         Query OWNER Details
     ======================================*/
  $qoW = "SELECT * FROM `owner membership` WHERE `IdNo` IN(SELECT `Owner` FROM `trucks` WHERE `Id`='$truckId')";
  if($roW = mysqli_query($dbc, $qoW)){

  while($owner = mysqli_fetch_array($roW))
  { 
  $firstname = $owner['Firstname'];
  $lastname = $owner['Lastname'];
  $idno=$owner['IdNo'];
  $email = $owner['Email'];
  $mobile = $owner['tel'];

    }

  }else{
    echo "failed";
  }

}else{
  die("No truck selected");
}

 


  ?>
<!-- ui-dialog -->
<div id="dialog" title="Leaving so soon??">
  <p>You are about to close <?php echo $truckName. " ". $truckNumber ?>. Are you sure you want to leave now??</p>
  <p><button type="button" class="btn btn-default" id="endModal">Close</button>
  <button type="button" class="btn btn-primary" data-dismiss="modal" id="exit">Leave Now!!</button>
  </p>
</div>
  <div class="col-md-12">
    <div class="thumbnail">

      <img src="<?php if ($truckImage=='empty') {
        echo "assets/images/noPic.jpg";
      }else{                
        echo $truckImage;
        } ?>" alt="..." width="50%">
      <div class="caption">
       <div class="col-sm-2 col-md-5"> 
       <h3><?php echo $truckName. " ". $truckNumber ?></h3>
        <p>A <?php echo $truckCategory; ?> Truck Operating in <strong><?php echo $truckArea; ?>.  </strong><br>
         category - <?php echo $truckCategory; ?><br>
         Company - <?php echo $truckCompany; ?><br>
          </p><hr>
          <u>Owner contact details</u>
          <h4><img src="assets/icons/worker.png"> Name: - <?php echo $firstname. " ". $lastname ?></h4>
          <h4><img src="assets/icons/new_message.png"> Email: - <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></h4>
          <h4><img src="assets/icons/blackberry.png"> CellPhone:  +254<?php echo "$mobile"; ?></h4><hr>

       </div>

       <div class="col-sm-2 col-md-7">
          <form class="form-horizontal" id="requestForm<?php echo $truckId; ?>" role="form">
            <div class="form-group">
              <label for="inputEmail1" class="col-lg-4 control-label">Cargo To Transport:</label>
              <div class="col-lg-8">
                <input type="email" class="form-control" id="inputCargo" placeholder="Cargo/Type To Transport">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail1" class="col-lg-4 control-label">Pick-Up Address:</label>
              <div class="col-lg-8">
                <input type="email" class="form-control" id="inputPickUpAddress" placeholder="Pick-Up Address/Locaton">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail1" class="col-lg-4 control-label">Drop-off Address</label>
              <div class="col-lg-8">
                <input type="email" class="form-control" id="inputDropOffAddress" placeholder="Drop-off Address/Location">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail1" class="col-lg-4 control-label">Pick-Up Date:</label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="datepicker" placeholder="Pick-Up Date(To Re-enable click outside)" readonly>
              </div>
            </div>                                    
            <div class="form-group">
              <label for="inputPassword1" class="col-lg-4 control-label">Expected transportation time:</label>
              <div class="col-lg-8">
                <input type="time" class="form-control" id="inputExpectedTime" placeholder="Expected total transportation time">
              </div>
            </div>
            <div class="form-group">
                  <div class="checkbox col-md-offset-3 col-lg-9 ">
                    <label class="control-label">
                      <input type="checkbox"> By submitting this form, I give consent to TrailSited and its partners to contact me by telephone or email through the contacts I have provided to receive information about TrailSited offers.
                    </label>
                  </div>
            </div>            
              <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                  <button id="reserveNow<?php echo $truckId; ?>" class="btn btn-success">Reserve Now!!</button>
                  <div id="reserveStatus<?php echo $truckId; ?>"></div>
                </div>
              </div>            
          </form>
       </div>

        <p><a href="home.php" class="btn btn-primary">Home</a> <button class="btn btn-default" id="dialog-link" >Close</button></p>
      </div>
    </div>
  </div>
</div> 
<!-- Modal -->




<?php 
$qt = "SELECT DATE_FORMAT(`pickUpDate`, '%Y/%m/%d') FROM `jobrequest` WHERE `truckNumber`= '$truckNumber' ";
$r = mysqli_query($dbc, $qt);

?>
<script type="text/javascript">


$(document).ready(function(){
 /*reserve The truck Now!!!*/
 $('#reserveNow<?php echo $truckId; ?>').click(function(e){
  e.preventDefault();
   var truckNumber = "<?php echo $truckNumber; ?>";
   var cargo = $('#inputCargo').val();
   var pickUpAddress = $('#inputPickUpAddress').val();
   var dropOffAddress = $('#inputDropOffAddress').val();
   var pickupDate = $('#datepicker').val();

   var expectedTime = $('#inputExpectedTime').val();
   var id = "<?php echo $id_no; ?>";

 if (cargo=='' || pickUpAddress==''|| dropOffAddress==''|| pickupDate==''|| expectedTime==''|| id=='') {
  $("#reserveStatus<?php echo $truckId; ?>").text('Fill all text fields with valid data!!');
  $("#reserveStatus<?php echo $truckId; ?>").css('backgoround-color','#FFE4C4').css('color','#DC143C').css('font-weight','bold');
 }else{
   $.post('config/trucks/reserve.php',{truckNumber1:truckNumber,cargo1:cargo, pickUpAddress1:pickUpAddress,dropOffAddress1:dropOffAddress, pickupDate1:pickupDate,expectedTime1:expectedTime,id1:id},function(data){
      $("#reserveStatus<?php echo $truckId; ?>").html(data);
      $('#requestForm<?php echo $truckId; ?>')[0].reset();
   });
 }
 });

  /*Close button*/
    $('#exit').click(function(){ 
     $('#dialog').dialog( "close" );     
      $('#hom').load('modules/Trucks/view.php');
      $('.backdrop').removeClass('in');
    });

    /*End modal button*/
    $('#endModal').click(function(){
      $('#dialog').dialog( "close" );
    });


  $(function(){/*Highlight reserved dates*/
 var availableDates = [<?php while($arr = mysqli_fetch_assoc($r)) { $time= $arr["DATE_FORMAT(`pickUpDate`, '%Y/%m/%d')"]; echo "'"; echo $time; echo "'";echo ","; } ?>];
 var days = {};
 for(i=0; i<availableDates.length;i++){
    days [new Date(availableDates[i])]=new Date(availableDates[i]);
 }


    jQuery('#datepicker').datepicker({
        dateFormat: 'yy-mm-dd',  minDate:0, maxDate: '+3m', showButtonPanel:true,
        beforeShowDay:function(date){
                   
                   var availday = days[date];              
                    if (availday) {
                        return[false, "event","Already booked"];
                    }else{
                        return[true, "other",""];
                    }
                 }  
    });
   })

$( "#dialog" ).dialog({
  autoOpen: false,
  width: 400,
  position: {my: 'top', at: 'top+150'},
  closeOnEscape: false,
  modal:true

});

// Link to open the dialog
$( "#dialog-link" ).click(function( event ) {
  $( "#dialog" ).dialog( "open" );
  event.preventDefault();
});

     
});

</script>
