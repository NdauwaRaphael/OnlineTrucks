<div class="row">
<style type="text/css">
#tabs-left-vertical .ui-tabs-nav { 
    position: absolute; 
     padding-left: 0.5em;
    transform: translate(-100%,0%) rotate(-90deg); 
    transform-origin: 100% 0%; 

} 

#tabs-left-vertical .ui-tabs-nav li { 
    float: right; 
    background: #000;

} 

#tabs-left-vertical .ui-tabs-panel { 
    padding-left: 3.5em; 
} 
#tabs-left-vertical .ui-tabs-panel { 
    height: 100%; 
}	
</style>
  <script>
  $( function() {
    $( "#tabs-left-vertical" ).tabs();
  } );
  </script>

<article id="tabs-left-vertical" class="tabs ui-tabs ui-widget ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all ">
		<li class="ui-state-default ui-corner-top"><a href="#tabs-left-vertical-1">Active Requests</a></li>
		<li class="ui-state-default ui-corner-top"><a href="#tabs-left-vertical-2">Accepted Requests</a></li>
		<li class="ui-state-default ui-corner-top"><a href="#tabs-left-vertical-3">Declined Requests</a></li>
	</ul>
	<div id="tabs-left-vertical-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
<?php
@include('../../config/connect.php');
@include('../../config/session.php');
@include('../../config/query.php');
 $queryJobs = "SELECT * FROM `jobrequest` WHERE `customerId`='$id_no' AND `status` = 'Requested' ";
 if ($Jresult=mysqli_query($dbc, $queryJobs)) {
 	while ($job=mysqli_fetch_array($Jresult)) {
 		$cargo= $job['cargo'];
 		$truck = $job['truckNumber'];
 		$address = $job['pickUpAddress'];
 		$destination = $job['dropOffAddress'];
 		$date = $job['pickUpDate'];
 		$id = $job['Id'];



 		?>
					<script type="text/javascript">
					  $( function() {
					    $( "#dialog-message<?php echo $id; ?>" ).dialog({
					     autoOpen: false,
					      modal: true,
					      position: {my: 'top', at: 'top+150'},
					      width: 800,
					      buttons: {
					        Ok: function() {
					          $( this ).dialog( "close" );
					        }
					      }
					    });
					    $( "#opener<?php echo $id; ?>" ).on( "click", function() {
					      $( "#dialog-message<?php echo $id; ?>" ).dialog( "open" );
					    });					    
					  } );
					</script>

			<div class="col-md-4 col-sm-3" style="padding: 0.8em; margin: 0.02em;">
				<button style="padding: 0.9em; color: #2e7d32; border-radius: 15px;" id="opener<?php echo $id; ?>" class="ui-button ui-widget ui-corner-all"><span><img src="assets/icons/delivery.png"></span> <?php echo "$cargo Transport from $address to $destination by $truck"; ?>
				</button><br>
			</div>	 
				<div id="dialog-message<?php echo $id; ?>" title="JOB REQUEST DESCRIPTION">
				<p><span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;">
				 </span>
					    Job details and status
					  </p>
					  <p>
					    Currently <b>Requested truck <?php echo "$truck to transport $cargo from $address to $destination as at $date"; ?></b>.
					  </p>
					</div>

 		<?php
 	}
 }
?>


					 

 		
	</div>

	<div id="tabs-left-vertical-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
<?php
 $queryJobs = "SELECT * FROM `jobrequest` WHERE `customerId`='$id_no' AND `status` = 'Accepted' ";
 if ($Jresult=mysqli_query($dbc, $queryJobs)) {
 	while ($job=mysqli_fetch_array($Jresult)) {
 		$cargo= $job['cargo'];
 		$truck = $job['truckNumber'];
 		$address = $job['pickUpAddress'];
 		$destination = $job['dropOffAddress'];
 		$date = $job['pickUpDate'];
 		$id = $job['Id'];
 		$cost = $job['cost'];


 		?>
					<script type="text/javascript">
					  $( function() {
					    $( "#dialog-message<?php echo $id; ?>" ).dialog({
					     autoOpen: false,
					      modal: true,
					      position: {my: 'top', at: 'top+150'},
					      width: 800,
					      buttons: {
					        Ok: function() {
					          $( this ).dialog( "close" );
					        }
					      }
					    });
					    $( "#opener<?php echo $id; ?>" ).on( "click", function() {
					      $( "#dialog-message<?php echo $id; ?>" ).dialog( "open" );
					    });					    
					  } );
					</script>

			<div class="col-md-4 col-sm-3" style="padding: 0.8em; margin: 0.02em;">
				<button id="opener<?php echo $id; ?>" class="ui-button ui-widget ui-corner-all"><span><img src="assets/icons/information.png"></span> <?php echo "$cargo Transport from $address to $destination"; ?>
				</button><br>
			</div>	 
				<div id="dialog-message<?php echo $id; ?>" title="JOB REQUEST DESCRIPTION">
				<p><span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;">
				 </span>
					    Job details and status
					  </p>
					  <p>
					    Currently <b>Requested truck <?php echo "$truck to transport $cargo from $address to $destination as at $date"; ?></b>.
					  </p>
					  <p>The Job will cost <strong>$ <?=$cost; ?>
					  	
					  </strong> Make a point of paying before time</p>
					</div>

 		<?php
 	}
 }
?>

	</div>

	<div id="tabs-left-vertical-3" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
<?php
 $queryJobs = "SELECT * FROM `jobrequest` WHERE `customerId`='$id_no' AND `status` = 'Declined' ";
 if ($Jresult=mysqli_query($dbc, $queryJobs)) {
 	while ($job=mysqli_fetch_array($Jresult)) {
 		$cargo= $job['cargo'];
 		$truck = $job['truckNumber'];
 		$address = $job['pickUpAddress'];
 		$destination = $job['dropOffAddress'];
 		$date = $job['pickUpDate'];
 		$id = $job['Id'];


 		?>
					<script type="text/javascript">
					  $( function() {
					    $( "#dialog-message<?php echo $id; ?>" ).dialog({
					     autoOpen: false,
					      modal: true,
					      position: {my: 'top', at: 'top+150'},
					      width: 800,
					      buttons: {
					        Ok: function() {
					          $( this ).dialog( "close" );
					        }
					      }
					    });
					    $( "#opener<?php echo $id; ?>" ).on( "click", function() {
					      $( "#dialog-message<?php echo $id; ?>" ).dialog( "open" );
					    });					    
					  } );
					</script>

			<div class="col-md-4 col-sm-3" style=" margin: 0.02em; ">
				<button style="background: #f44336; padding: 0.9em; color: #f3e5f5; border-radius: 15px;" id="opener<?php echo $id; ?>" class="ui-button ui-widget ui-corner-all"><span><img src="assets/icons/error.png"></span> <?php echo "$cargo Transport from $address to $destination"; ?>
				</button><br>
			</div>	 
				<div id="dialog-message<?php echo $id; ?>" title="JOB REQUEST DESCRIPTION">
				<p><span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0; ">
				 </span>
					    Job details and status
					  </p>
					  <p>
					    Currently <b>Requested truck <?php echo "$truck to transport $cargo from $address to $destination as at $date"; ?></b>.
					  </p>
					</div>

 		<?php
 	}
 }
?>

	</div>
</article>	
</div>