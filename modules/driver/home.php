<?php
include('../../config/connect.php');
include('config/driver_conf.php');

if (!isset($_SESSION['driver_email']) && empty($_SESSION['driver_email'])) {
  header("location: ../../index.php");
}
?>

<html>
<head>
<title>Online Trucks Reservation</title>
<?php include('../../config/css.php'); ?>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">
<style type="text/css">
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url('http://themes.googleusercontent.com/static/fonts/opensans/v5/cJZKeOuBrn4kERxqtaUH3T8E0i7KZn-EPnyo3HZu7kw.woff') format('woff');
}

tr, td {
  font-family: 'open sans',arial,sans-serif;
}


/* Profile container */
.profile {
  margin: 3px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 30%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
</style>
<?php include("../../config/js.php"); ?>
</head>



<body>
	<div id="navBg">
	<div class="nav">
       <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle " data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                  <div class="truckImageLogo">
                    <img src="../../assets/icons/truck.png"  >Online Trucks
                  </div>
              </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li ><a href="home.php" >Home</a></li>
            	<li><a href="#about">About Us</a></li>
            	<li><a href="#services">Services</a></li>
            	<li><a href="#faqs">FAQs</a></li>
            	<li><a href="#contacts">Contacts</a></li>
             </ul>
             <ul class="nav navbar-nav navbar-right">
             	<li class="active">
                 <a href="#acct"><button class="btn btn-success" id="btnProf"><span class="glyphicon glyphicon-user"> </span> <?php echo $fname." ".$lname; ?></button></a>
             	</li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-wrench"></span> Options <b class="caret"></b></a>
        <ul class="dropdown-menu" id="navBg">
          <li><a href="#">Profile</a></li>
          <li><a href="#">Settings</a></li>
          <li><a href="#">Something</a></li>
          <li><a href="#">Separated link</a></li>
          <li><a href="config/logout.php"><span class="glyphicon glyphicon-off"></span> logout</a></li>
        </ul>
      </li>

             </ul>                   	
	</div>
</div>
</div>
</div>
<!--end navigation area-->

<div class="row">
  
</div>

<div class="row">
<div class="container">
  <div class="col-md-6">
    <div class="panel panel-success">
      <div class="panel-heading">Job requests</div>
      <div class="panel-body">
        <table class="table table-condensed">
        <thead>
          <th>Date:</th>
          <th>Time:</th>
          <th>Cargo:</th>
          <th>From:</th>
          <th>To:</th>
          <th>Action:</th>
        </thead>
        <?php
          $select_jobs = "SELECT * FROM `jobrequest` WHERE `truckNumber`='$truck' AND `status`='Accepted' ";
          $res = mysqli_query($dbc, $select_jobs);

          while ($job = mysqli_fetch_array($res)) {
            $date = $job['pickUpDate'];
            $from = $job['pickUpAddress'];
            $to = $job['dropOffAddress'];
            $time = $job['expectedTime'];
            $cargo = $job['cargo'];
            $id = $job['Id'];

?>
<tr style="font-size: 0.8em;">
   <td><?= $date; ?></td>
   <td><?= $time; ?></td>
   <td><?= $cargo; ?></td>
   <td><?= $from; ?></td>
   <td><?= $to; ?></td>
   <td><button class="btn btn-success btn-sm" id="finish<?=$id; ?>">Finish</button></td>

</tr>
<script type="text/javascript">
  $("#finish<?=$id; ?>").click(function(){
    var id1 = "<?=$id; ?>";

    if (id1=='') {
      alert("No Job is selected");
    }else{
      $.post("config/finish_job.php",{id:id1},function(data){
         alert(data);
         window.location.href="home.php";
      });
    }
  })

</script>    

<?php            
          }

        ?>
</table>        
      </div>
</div>
  </div>

  <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">Driver Profile</div>
        <div class="panel-body">


    <div class="profile">
    <div class="col-md-12">
      <div class="profile-sidebar">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
          <img src="images/woman-driver1.jpg" class="img-responsive" alt="">
        </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
          <div class="profile-usertitle-name">
            <?= $fname . " " . $lname ?>
          </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <!-- SIDEBAR BUTTONS -->
        <div class="profile-userbuttons">
          <button type="button" class="btn btn-success btn-sm">Change Profile</button>
        </div>
        <!-- END SIDEBAR BUTTONS -->
        <!-- SIDEBAR MENU -->
        <div class="profile-usermenu">
          <ul class="nav">
            <li class="active">
              <a href="#">
              <i class="glyphicon glyphicon-home"></i>
              Overview </a>
            </li>
            <li>
              <a href="#">
              <i class="glyphicon glyphicon-user"></i>
              <?= $fname . " " . $lname ?> </a>
            </li>

            <li>
              <a href="#" target="_blank">
              <i class="glyphicon glyphicon-envelope"></i>
              <?= $email; ?> </a>
            </li>

            <li>
              <a href="#" target="_blank">
              <i class="glyphicon glyphicon-globe"></i>
              <?= $idno; ?> </a>
            </li>

            <li>
              <a href="#" target="_blank">
              <i class="glyphicon glyphicon-ok"></i>
              Active </a>
            </li>
            <li>
              <a href="#">
              <i class="glyphicon glyphicon-flag"></i>
              <?= $truck; ?> </a>
            </li>

          </ul>
        </div>
        <!-- END MENU -->
      </div>
    </div>

  </div>
</div>
          

      </div>
  </div>
  </div>
</div>



<footer>
  <div class="container">
    <div class="row">
        <div class="col-sm-2">
            <h6>copyright: &copy; 2016 Online Trucks App Develop. All Rights Reserved</h6>
        </div><!--end col-small-2-->

        <div class="col-sm-4">
              <h6>About Us</h6>
              <p>Know more about us</p>
        </div><!--end col-small-4-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
             <p><span class="glyphicon glyphicon-heart"> </span> Managed and Designed by DITS</p>
        </div>

    </div><!--end row-->

  </div><!--end footer container-->

</footer>



</body>

<script type="text/javascript">
$(document).ready(function(){
       $('#viewTruck').load('trucks/ownerView.php');
});

</script>
</html>

