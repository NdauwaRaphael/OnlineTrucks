<html>
<head>
<title>Online Trucks Reservation</title>
<?php @include('../../config/css.php'); ?>
<?php @include('config/css.php'); ?>
<?php @include('../config/css.php'); ?>
<style type="text/css">

#goHome:hover { font-size:16px; color: red; border-bottom: 1px solid red; cursor: pointer;}
</style>
</head>

<body>
  <div id="navBg">
  <div class="nav">
       <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                  <div class="truckImageLogo">
                    <image src="../../assets/icons/truck.png"  >Online Trucks
                  </div>
              </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                
              <li><a href="#about">About Us</a></li>
              <li><a href="#services">Services</a></li>
              <li><a href="#faqs">FAQs</a></li>
              <li><a href="#contacts">Contacts</a></li>
             </ul>
             <ul class="nav navbar-nav navbar-right">
              <li class="active">
                 <a href="#acct"><button id="btnReg" class="btn btn-success">My Account</button></a>
              </li>
             </ul>                    
  </div>
</div>
</div>
</div>
<!--end navigation area-->

<div class="row">
<div class="container">

<div class="panel">
  <div class="panel-heading" id="sidedbar">
    <div id="saveStatus"></div>
    <center><H2><img src="../../assets/icons/add_user-48.png" class="img-circle">Register here Truck Owner. Thank You for choosing TrailSited!!</H2></center>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" role="form" id="registerMe">
        
          <div class="form-group">
          <label for="inputEmail1" class="col-lg-2 control-label">First Name</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="inputFname" placeholder="First Name">
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail1" class="col-lg-2 control-label">Last Name</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="inputLname" placeholder="Last Name">
          </div>
        </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Gender</label>
    <div class="col-lg-10">
        <select id="gender" name="gender" class="form-control">
          <option>-------------------------SELECT GENDER HERE--------------------------------</option>
          <option value="Female">Female</option>
          <option value="Male">Male</option>
          <option value="Other">Other</option>
        </select>
    </div>
  </div>  
        <div class="form-group">
          <label for="inputEmail1" class="col-lg-2 control-label">Id Number</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="inputIdno" placeholder="National Identification Number">
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail1" class="col-lg-2 control-label">Mobile Phone</label>
          <div class="col-lg-10">
            <input type="number" class="form-control" id="inputMobile" placeholder="Mobile Phone Number">
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail1" class="col-lg-2 control-label">Email</label>
          <div class="col-lg-10">
            <input type="email" class="form-control" id="inputEmail" placeholder="Email" onchange="email_validate(this.value);">
            <p id="emailstatus"></p>
          </div>
        </div>


        <div class="form-group">
          <label for="inputPassword1" class="col-lg-2 control-label">Password</label>
          <div class="col-lg-10">
            <input type="password" class="form-control" id="inputPassword1" placeholder="Password">
          </div>
        </div>

        <div class="form-group">
          <label for="inputPassword1" class="col-lg-2 control-label">Repeat Password</label>
          <div class="col-lg-10">
            <input type="password" class="form-control" id="inputPassword2" placeholder="Confirm Password" onkeyup="checkPass(); return false;">
            <p id="message"></p>
          </div>
        </div>
         </tr>   

        <div class="form-group">
          <div class="col-lg-offset-2 col-lg-10">
            <button id="regOwnerBtn" class="btn btn-success btn-lg">Sign in</button>
          </div>
        </div>

       </form> 

    <div class="alert alert-info">
      <a href="../../index.php"><strong id="goHome"><span class="glyphicon glyphicon-home"></span>
        Go Back home</strong></a>
    </div>      
  </div>
</div>



   
</div>
</div>

</body>
<?php @include "../../config/js.php"; ?>
<?php @include "config/js.php"; ?>
<?php @include "../config/js.php"; ?>
<script type="text/javascript">

/*Validating*/
function checkPass()
{
       
    
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('inputPassword1');
    var pass2 = document.getElementById('inputPassword2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('message');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match";

        $('#regOwnerBtn').prop('disabled', false);
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.

        $('#regOwnerBtn').prop('disabled', true);
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
} 

// validates text only
function Validate(txt) {
    txt.value = txt.value.replace(/[^a-zA-Z-'\n\r.]+/g, '');
}
// validate email
function email_validate(email)
{
var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
   var status = document.getElementById("emailstatus");
    if(regMail.test(email) == false)
    {    
    document.getElementById("emailstatus").innerHTML    = "<span class='warning'>Email address is invalid .</span>";
        status.style.color = "#f44336";
        $('#regOwnerBtn').prop('disabled', true);
        $('#regOwner').prop('disabled', true);
    }
    else
    {
    document.getElementById("emailstatus").innerHTML	= "<span class='valid'>Email address is Valid!</span>";	
        status.style.color = "#a5d6a7";
        $('#regOwnerBtn').prop('disabled', false);
        $('#regOwner').prop('disabled', false);
    }
}   

</script>

</html>