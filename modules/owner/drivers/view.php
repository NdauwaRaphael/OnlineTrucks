

<table class="table table-condensed">
  <thead>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Id Number</th>
      <th>Truck</th>
      <th>Email</th>
      <th>Password</th>
      <th>Action</th>
      <th>Config</th>
  </thead>

<?php
$dbc = mysqli_connect('localhost', 'root', '', 'online trucks') OR die('could not connect to database because'. mysqli_connect_error()); 

	$qD = "SELECT * FROM `driver`";
	if($rD = mysqli_query($dbc, $qD)){

	while($driver = mysqli_fetch_array($rD))
	{ 
$id = $driver['Id'];        
$firstname = $driver['firstname'];
$lastname = $driver['lastname'];
$idno = $driver['idnumber'];
$truck = $driver['trucknumber'];
$email = $driver['email'];
$password = $driver['password'];
        
?>

  <tbody>
      <td><?= $firstname; ?></td>
      <td><?= $lastname; ?></td>
      <td><?= $idno; ?></td>
      <td><?= $truck; ?></td>
      <td><?= $email; ?></td>
      <td><?= $password; ?></td>
      <td><button class="btn btn-warning btn-sm" id="Delete<?= $id; ?>" type="button"><span class="glyphicon glyphicon-off"></span> Delete</button></td>
      <td><button data-toggle="modal" data-target="#edit<?= $id; ?>" class="btn btn-info btn-sm" type="button"><span class="glyphicon glyphicon-cog"></span> Edit</button></td>
  <tbody>



<div class="modal fade " id="edit<?= $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title">Edit <?= $firstname ." ". $lastname; ?></h4>
                </div>
                <div class="modal-body">
<form>
  <div class="form-group">
    <label >First Name</label>
    <input type="text" value="<?= $firstname; ?>" class="form-control" id="fname" placeholder="">
  </div>

  <div class="form-group">
    <label >Last Name</label>
    <input type="text" value="<?= $lastname; ?>" class="form-control" id="lname" placeholder="">
  </div>

  <div class="form-group">
    <label >Identity Number</label>
    <input type="text" class="form-control" value="<?= $idno; ?>"  id="idno" placeholder="">
  </div>

  <div class="form-group">
    <label >Email address</label>
    <input type="email" class="form-control" value="<?= $email; ?>"  id="email" placeholder="">
  </div>

  <div class="form-group">
    <label for="">Password</label>
    <input type="text" class="form-control" value="<?= $password; ?>" id="password" placeholder="">
  </div> 
   <button type="submit" class="btn btn-default">Submit</button>         
</form>  
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#Delete<?= $id; ?>").click(function(){
    var id1 = "<?= $id; ?>";
     $.post("config/delete_driver.php",{id:id1},function(data){
      alert(data);
      $('#viewTruck').load('drivers/view.php');      
     })
  })
</script>
  <?php
    }
    }
  ?>
</table>