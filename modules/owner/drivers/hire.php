<?php
if (getcwd()=='C:\xampp\htdocs\OnlineTrucks\modules\owner\drivers') {
include('../../../config/connect.php');
include('../../../config/session.php');
include('../../../config/queryOwner.php');  
}?>
<div class="panel panel-default">
  <div class="panel-heading"><center>Register a new driver</center></div>
  <div class="panel-body">
            <form class="form-horizontal" id="hire_form">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">First Name:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="firstname" placeholder="Add Driver's first name">
                </div>
              </div>
               <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Last Name:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="lastname" placeholder="Add Driver's Last Name">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Id Number:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="idno" placeholder="Add Driver's Id Number">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Truck Number:</label>
                <div class="col-sm-10">
                    <select class="form-control" id="truck">
                    <option VALUE=''>----[SELECT TRUCK]------</option>
                      <?php 
                        $tquery = "SELECT * FROM `trucks` WHERE `Owner`='$ownerIdNo' ";
                        $tr = mysqli_query($dbc, $tquery);
                          while($trucks = mysqli_fetch_array($tr)){
                             $plate = $trucks['truckNumber'];   
                         ?>
                        <option value="<?= $plate ?>" class="option"><?= $plate ?></option>
                         <?php } ?>
                    </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Email:</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="email" placeholder="Driver's Email">
                </div>
              </div>         
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" id="password" placeholder="Password">
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Confirm Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password">
                </div>
              </div>              
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> By registering this driver, you are choosing to assign all the responsibility of the assigned truck to the driver. The driver will be emailed the details once you hit register. check here if you understand these terms and choose to proceed.
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="button" id="add_driver_btn" class="btn btn-default">Register Now!!</button>

                  <div id="driver_status"></div>
                </div>
                
              </div>
              
            </form>
  </div>
</div>

<script>
    $("#add_driver_btn").click(function(){
      var Firstname = $("#firstname").val();
      var Lastname = $("#lastname").val();
      var Idno = $("#idno").val();
      var Truck = $("#truck").val();
      var Email = $("#email").val();
      var Password = $("#password").val();

      if(Firstname==''|| Lastname==''|| Idno=='' || Truck=='' || Email=='' || Password==''){
         $("#driver_status").html('<div class="alert alert-danger" role="alert">Fill in all empty fields!</div>');
      }else{
        $.post("config/add_driver_config.php",{firstname:Firstname, lastname:Lastname, idno:Idno, truck:Truck, email:Email, password:Password}, function(data){
          if (data=='success') {
            $("#driver_status").html('<div class="alert alert-success" role="alert">Driver have been added successfuly</div>');
            $("#hire_form")[0].reset();            
          }else{
            $("#driver_status").html(data)
          }
        });
      }

    })
</script>

