<?php
include('../../config/connect.php');
include('../../config/session.php');
include('../../config/queryOwner.php');
include('../../config/check_active.php');



if (!ADMIN()) {
   header("location: ../../index.php");
}else{
  if (!admin_active()) {
  header("location: ../../error/error.php");
}
}

?>

<html>
<head>
<title>Online Trucks Reservation</title>
<?php include('../../config/css.php'); ?>
<style type="text/css">
#viewTruck {
  padding-top: 30px;
}
</style>
</head>



<body>
	<div id="navBg">
	<div class="nav">
       <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle " data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                  <div class="truckImageLogo">
                    <image src="../../assets/icons/truck.png"  >Online Trucks
                  </div>
              </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li ><a href="home.php" >Home</a></li>
            	<li><a href="#about">About Us</a></li>
            	<li><a href="#services">Services</a></li>
            	<li><a href="#faqs">FAQs</a></li>
            	<li><a href="#contacts">Contacts</a></li>
             </ul>
             <ul class="nav navbar-nav navbar-right">
             	<li class="active">
                 <a href="#acct"><button class="btn btn-success" id="btnProf"><span class="glyphicon glyphicon-user"> </span> <?php echo $fname." ".$lname; ?></button></a>
             	</li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-wrench"></span> Options <b class="caret"></b></a>
        <ul class="dropdown-menu" id="navBg">
          <li><a href="#">Profile</a></li>
          <li><a href="#">Settings</a></li>
          <li><a href="#">Something</a></li>
          <li><a href="#">Separated link</a></li>
          <li><a href="../../config/logOwnerOut.php"><span class="glyphicon glyphicon-off"></span> logout</a></li>
        </ul>
      </li>

             </ul>                   	
	</div>
</div>
</div>
</div>
<!--end navigation area-->
<div class="bar">
    <a class="toggle" href="#"></a>
</div>

    <div class="">

      </div>



<div class="hereContent">
  
  <div id="hom">
    <div class="row">
<div class="col-lg-2" >

<!--User Profile-->
           <span class="hamburger" onclick="openNav()">TOGGLE NAVIGATION&#9776;</span>
             <div id="mySidenav" class="sidenav">
              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>      
      <div class="col-md-12" id="sider">
       <div class="thumbnail" id="thumb" >
              <?php if ($gender=='Male') {
              ?>
                <img  src="../../assets/images/users/user-male.jpg" alt="..." width="45%">
                <?php 
                  }else{
                    ?>
                     <img src="../../assets/images/users/user-female.jpg" alt="..." width="40%">
                    <?php
                    } 
                ?>
                <div class="caption">
                  <center>
                  <h5><?php echo $fname." ".$lname; ?></h5>
                  <p><a href="#"><i class="fa fa-circle text-success"></i> Online</a></p>
                 <!-- <p><a href="#" class="btn btn-primary btn-xs" role="button">Profile</a> <a href="#" class="btn btn-default btn-xs" role="button">Logout</a></p>-->
                  </center>
                </div>
              </div>
            </div> 


      <!--Cartegory 1-->
          <div class="panel-group" id="accordion">
                              <div class="panel" id="sider">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                      <img src="../../assets/icons/interstate_truck.png"> Trucks  <span class="glyphicon glyphicon-chevron-down"></span>
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                  <div class="panel-body">
                                   <ul class="list-unstyled">
                        <li><a type="button" id="tAdd" >Add-New Truck</a></li>
                        <li><a type="button" id="tView">View-Truck List</a></li>
                       </ul>
                                  </div>
                                </div>
                              </div>
                            <!--end cat-->

      <!--Cartegory 2-->
                              <div class="panel" id="sider">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                      <img src="../../assets/icons/line_chart.png"> Jobs <span class="glyphicon glyphicon-chevron-down"></span>
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                  <div class="panel-body">
                                   <ul class="list-unstyled">
                        <li><a type="button" id="jNew">New Jobs</a></li>
                        <li><a type="button" id="jPView" >Pending Jobs</a></li>
                        <li><a type="button" id="JcView" >Completed jobs</a></li>
                       </ul>
                                  </div>
                                </div>
                              </div>
                            <!--end cat--> 

      <!--Cartegory 3-->
                              <div class="panel" id="sider">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                      <img src="../../assets/icons/driver.png"> Drivers  <span class="glyphicon glyphicon-chevron-down "></span>
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                  <div class="panel-body">
                                   <ul class="list-unstyled">
                        <li><a type="button" id="dAdd" >Hire</a></li>
                        <li><a type="button" id="dView" >View Drivers</a></li>
                       </ul>
                                  </div>
                                </div>
                              </div>
                            <!--end cat--> 
      <!--Cartegory 3-->
                              <div class="panel" id="sider">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                      <img src="../../assets/icons/library.png"> Company <span class="glyphicon glyphicon-chevron-down"></span>
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                  <div class="panel-body">
                                   <ul class="list-unstyled">
                        <li><a type="button" id="cAdd" >New Company</a></li>
                        <li><a type="button" id="cView" >View Details</a></li>
                       </ul>
                                  </div>
                                </div>
                              </div>


      <!--Cartegory 3-->
                              <div class="panel" id="sider">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                      <img src="../../assets/icons/bullish.png"> Reports <span class="glyphicon glyphicon-chevron-down"></span>
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse">
                                  <div class="panel-body">
                                   <ul class="list-unstyled">
                        <li><a type="button" id="truck_repo" >Trucks Report</a></li>
                        <li><a type="button" id="job_repo" >Jobs Report</a></li>
                        <li><a type="button" id="finance_repo">Financial Report</a></li>
                       </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!--end cat-->                 

                 
                                                                
</div>
</div>

<div class="col-md-10">
  <div id="viewTruck">
 <?php include('trucks/ownerView.php'); ?>
  </div>

</div>

    </div>
  </div>

  
</div>

<footer>
  <div class="container">
    <div class="row">
        <div class="col-sm-2">
            <h6>copyright: &copy; 2016 Online Trucks App Develop. All Rights Reserved</h6>
        </div><!--end col-small-2-->

        <div class="col-sm-4">
              <h6>About Us</h6>
              <p>Know more about us</p>
        </div><!--end col-small-4-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
             <p><span class="glyphicon glyphicon-heart"> </span> Managed and Designed by DITS</p>
        </div>

    </div><!--end row-->

  </div><!--end footer container-->

</footer>



</body>
<?php include("../../config/js.php"); ?>
<script type="text/javascript">
$(document).ready(function(){
       $('#viewTruck').load('trucks/ownerView.php');
       
});

</script>
</html>