<?php
@include('../../../config/connect.php');
@include('../../../config/session.php');

/*=======================================
Read a value from text file
========================================*/
$fileName = "Read/image.txt";
if (file_exists($fileName) && filesize($fileName)>0) {  
  $handleMe = fopen($fileName, 'r');
  $truckNumber= fread($handleMe, filesize($fileName));


 /*=====================================
         Query Owner Individual Trucks Details
     ======================================*/ 

  $qT = "SELECT * FROM `trucks` WHERE `truckNumber`='$truckNumber' ";
  if($rT = mysqli_query($dbc, $qT)){

  while($truck = mysqli_fetch_array($rT))
  { 
  $tId = $truck['Id'];
  $tName = $truck['truckName'];
  $tNumber = $truck['truckNumber'];
  $company=$truck['company'];
  $area = $truck['areaOfOperation'];
  $capacity = $truck['cargoCapacity'];
  $category = $truck['cargoCategory'];
  $image = $truck['imagePath'];

    }

  }else{
    echo "failed";
  }
  }else{ echo "No text";} 


  ?>

<div class="row">
  <div class="col-md-7">
    <div class="thumbnail">
      <img src="holder.js/300x200" alt="...">
      <div class="caption">
        <h3><?php echo $tName. " ". $tNumber ?></h3>
        <p>...</p>
        <!--==================================-->
			<form id="uploadForm" action="../../config/uploadTruckImage.php" method="post">
			<div id="targetLayer">No Image</div>
			<div id="uploadFormLayer">
			<label>Upload Image File:</label><br/>
			<input name="truckImage" type="file" class="inputFile" />
			<input type="submit" value="Submit" class="btnSubmit" />
			</form>

        <p><a href="#" class="btn btn-primary">Save</a> <a href="#" class="btn btn-default">Skip</a></p>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function (e) {
  $("#uploadForm").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
          url: "../../config/uploadTruckImage.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
          cache: false,
      processData:false,
      success: function(data)
        {
      $("#targetLayer").html(data);
        },
        error: function() 
        {
        }           
     });
  }));
});
</script>