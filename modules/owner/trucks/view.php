<div class="row">
<?php
@include('../../../config/connect.php');
@include('../../../config/session.php');

/*=======================================
Read a value from text file
========================================*/
$fileName = "Read/write.txt";
if (file_exists($fileName) && filesize($fileName)>0) {  
  $handleMe = fopen($fileName, 'r');
  $truckId= fread($handleMe, filesize($fileName));


 /*=====================================
         Query Owner Individual Trucks Details
     ======================================*/ 

  $qT = "SELECT * FROM `trucks` WHERE `Id`='$truckId' ";
  if($rT = mysqli_query($dbc, $qT)){

  while($truck = mysqli_fetch_array($rT))
  { 
  $tId = $truck['Id'];
  $tName = $truck['truckName'];
  $tNumber = $truck['truckNumber'];
  $company=$truck['company'];
  $area = $truck['areaOfOperation'];
  $capacity = $truck['cargoCapacity'];
  $category = $truck['cargoCategory'];
  $image = $truck['imagePath'];

    }

  }else{
    echo "failed";
  }
  }else{ echo "No text";} 


  ?> 

  <div class="col-md-11">
    <div class="thumbnail">
    <div class="navbar navbar-default">
    <div class="contained">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#" class="navbar-btn" id="goHomeBtn"><span class="glyphicon glyphicon-home"></span> GO HOME</a></li>
      <li><button type="button" class="btn btn-link"></button></li> 
      <li><a href="#" class="btn-default btn-xs" id="goCloseBtn"><span class="glyphicon glyphicon-remove"></span> CLOSE</a></li>
    </ul>
    </div>
    </div>
      <div class="caption">
      	<div class="col-sm-2 col-md-6">
       <h3><?php echo $tName. " ". $tNumber ?></h3>

				<table class="table table-hover">
				         	<tr>
				                <td>Truck Name:</td>
				                <td><input type="email" class="form-control disabled" id="inputCargo" value="<?php echo $tName; ?>" placeholder="Truck Name"></td>
				         	</tr>

				         	<tr>
				                <td>Truck Number:</td>
				                <td><input type="email" class="form-control" id="inputPickUpAddress" value="<?php echo $tNumber; ?>" placeholder="Truck Number" disabled></td>
				         	</tr>

				         	<tr>
				                <td>Truck Company:</td>
				                <td> <input type="email" class="form-control" id="inputDropOffAddress" value="<?php echo $company; ?>" placeholder="Truck Company" disabled></td>
				         	</tr>

				         	<tr>
				                <td>Area of Operation:</td>
				                <td><input type="text" class="form-control" id="inputPickUpDate" value="<?php echo $area; ?>" placeholder="Area of Operation" disabled></td>
				         	</tr>

				         	<tr>
				                <td>Truck Capacity:</td>
				                <td><input type="text" class="form-control" id="inputExpectedTime" value="<?php echo $capacity; ?>" placeholder="Cargo Capacity the Truck can transport" disabled></td>
				         	</tr>

				         	<tr>
				                <td>Category</td>
				                <td><input type="text" class="form-control" id="inputExpectedTime" value="<?php echo $category; ?>" placeholder="Cargo Category the Truck Transports" disabled></td>
				         	</tr>

				         	<tr>
				                <td>Truck Driver:</td>
				                <td><?php echo "Driver"; ?></td>
				         	</tr>         	         	
				         </table>
                  <button id="modifyNow<?php echo $tId; ?>" class="btn btn-info"><span class="glyphicon glyphicon-pencil"> </span> Modify Now!!</button>
                  <div id="modifyStatus<?php echo $tId; ?>"></div>	
               		                   
         </div>

        <div class="col-sm-2 col-md-6">
			    <div class="thumbnail" id="targetLayer">
			      <img src="<?php if ($image=='empty') {
                  echo "../../assets/images/noPic.gif";
                }else{
                  echo "../"."../"; echo $image;
                } ?>" alt="..." width="100%">
			    </div>            	
            <h4>Modify The Truck Image. Upload here</h4>
            <form id="uploadForm" action="../../config/uploadTruckImage.php" method="post">
				  <div class="form-group">
				    <label for="exampleInputFile">Truck image</label>
				    <input type="file" name="truckImage" id="inputImagePath">
				    <p class="help-block">It is optional to add truck image, but it is vital for customers to see the trucks.</p>
				  </div>
				  <button class="btn btn-success" id="addTruckBtn">Add Now</button>                
            </form>
       
       </div><hr> 
          <div class="row">
            <h5>Schedule</h5>
            <div id="Timetable"></div>
          </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function (e) {
  $("#uploadForm").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
          url: "../../config/uploadTruckImage.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
          cache: false,
      processData:false,
      success: function(data)
        {
      $("#targetLayer").html(data);
        },
        error: function() 
        {
        }           
     });
  }));

  /*Close Buttons*/
  $('#goHomeBtn').click(function(){
    $('#viewTruck').hide().load('trucks/ownerView.php').fadeIn('slow');
  })

  $('#goCloseBtn').click(function(){
    $('#viewTruck').hide().load('trucks/ownerView.php').fadeIn('slow');
  })  

});

  /*Caledar (Timeteble) displaying requeted activities*/
$(function(){

  jQuery('#Timeteble').datepicker({
    dateFormat: 'yy-mm-dd',
    inline: true
  });  
})
</script>