<?php
if (getcwd()=='C:\xampp\htdocs\OnlineTrucks\modules\owner\trucks') {
include('../../../config/connect.php');
include('../../../config/session.php');
include('../../../config/queryOwner.php');  
}?>

<!--here is a panel encampassing codes to add a truck-->
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Add a new truck here</h3>
		  </div>
		  <div class="panel-body alert alert-success">
        <div id="saveStatus"></div>
		    
<form role="form" id="addTruckFrm">
  <div class="form-group">
    <label for="exampleInputEmail1">Truck Name</label>
    <input type="text" class="form-control" id="inputTruckName" placeholder="Truck Name">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Truck Number</label>
    <input type="text" class="form-control" id="inputTruckNumber" placeholder="Truck Number">
  </div> 
  <div class="form-group">
    <label for="exampleInputPassword1">Company</label>
    <select id="inputCompany" class="form-control input-sm" center>
      <option value="0">-----Company----</option>
          <?php 
               $Owner = $_SESSION['adminUsername'];
                $result = mysqli_query($dbc, "SELECT * FROM `truck_company` WHERE `Owner`='$Owner' ORDER BY `truck_company`.`Name` ASC");
                while ($nem = mysqli_fetch_array($result) ) {
                  $queriednem = $nem['Name'];
               ?>
                <option value="<?php echo "$queriednem"; ?>"><?php echo "$queriednem"; ?></option>
                <?php
                }
          ?>      
      <option value="Private owned">Private owned</option>
    </select>
  </div>     
  <div class="form-group">
    <label for="exampleInputPassword1">Area of operation</label>
    <input type="text" class="form-control" id="inputAreaOfOperation" placeholder="Area of operation">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Cargo Capacity</label>
    <input type="text" class="form-control" id="inputCargoCapacity" placeholder="Cargo Capacity">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Cargo Category</label>
    <select id="inputCargoCategory" class="form-control input-sm" center>
      <option value="0">-----Cargo category----</option>
          <?php 
                $resul = mysqli_query($dbc, "SELECT * FROM `truck_categories` ORDER BY `truck_categories`.`category` ASC");
                while ($cat = mysqli_fetch_array($resul) ) {
                  $queriedCategory = $cat['category'];
               ?>
                <option value="<?php echo "$queriedCategory"; ?>"><?php echo "$queriedCategory"; ?></option>
                <?php
                }
          ?>      

    </select>
  </div>    
  <div class="form-group">
    <label for="exampleInputFile">Truck image</label>
      <div class="checkbox">
        <label>
          <input type="checkbox" id="truckImage" value="empty">
          If you want to add truck image&mdash;Check to add now
        </label>
      </div>
    <p class="help-block" id="uploadStatus">It is optional to add truck image, but it is vital for customers to see the trucks.</p>
  </div>
  <button class="btn btn-success" id="addTruckBtn">Add Now</button>
</form>

<div class="progress progress-striped">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
    <span class="sr-only">40% Complete (success)</span>
  </div>
</div>
		  </div>
		</div>


  <!-- Modal -->
  <div class="modal fade" data-backdrop="false" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Truck add Notification!!</h4>
        </div>
        <div class="modal-body">
            <div class="alert alert-success">Well done! You successfully Registered a new truck. Click proceed to view your trucks..</div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="proceedHome">Proceed to main view</button>
           <script type="text/javascript">
             $('#proceedHome').click(function(){
               $('#viewTruck').load('trucks/ownerView.php');
             })
          </script>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal --> <div id="thisBody" class=""></div>   
<!--add a truck panel ends here-->
<script type="text/javascript">


 $('#addTruckBtn').click(function(e){
   e.preventDefault();   
   var owner = "<?php echo $ownerIdNo; ?>";
   var truckName = $('#inputTruckName').val();
   var truckNumber = $('#inputTruckNumber').val();
   var truckCompany = $('#inputCompany').val();
   var truckArea = $('#inputAreaOfOperation').val();
   var truckCapacity = $('#inputCargoCapacity').val();
   var truckCartegory = $('#inputCargoCategory').val();
   var truckImage= $('#truckImage').val();






   if (truckName==''|| truckNumber=='' || truckCompany=='' || truckArea=='' || truckCapacity=='' || truckCartegory=='' ||truckImage=='') 
    {
     alert('FILL IN ALL REQUIRED FIELDS!!!!');

    }else{

      $.post('../../config/addTruck.php',{owner1:owner, truckName1:truckName, truckNumber1:truckNumber, truckCompany1:truckCompany, truckArea1:truckArea, truckCapacity1:truckCapacity, truckCartegory1:truckCartegory, truckImage1:truckImage}, function(data){
                  $('#saveStatus').html(data);
                  $('#addTruckFrm')[0].reset();

                  $('#myModal').modal('toggle'); 
                  $('#myModal').on('shown.bs.modal', function () {
                      $("#thisBody").attr('class','modal-backdrop fade in');
                   }); 
                       
      })
    }

   });

</script>