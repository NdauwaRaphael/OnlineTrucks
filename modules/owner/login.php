<html>
<head>
<title>Online Trucks Reservation</title>
<?php @include('../../config/css.php'); ?>
<?php @include('config/css.php'); ?>
<?php @include('../config/css.php'); ?>
<style type="text/css">

#regOwner:hover { font-size:16px; color: red; border-bottom: 1px solid red; cursor: pointer;}
</style>
</head>

<body>

<div class = "jumbotron">
<div class="container">

 
<div class="row" id="reg">
<div class="col-md-4 col-md-offset-4">

<div id="response"></div>	

		<div class="panel panel-success">
		  <div class="panel-heading">
			<h3 class="panel-title">
                <?php if (getcwd()=='C:\xampp\htdocs\OnlineTrucks') {
                	?>
				<img id="ownerLoginIcon" src="assets/icons/checked_truck-48.png" >Truck Owner Loggin</h3>
                <?php	
                } elseif (getcwd()=='C:\xampp\htdocs\OnlineTrucks\modules\owner') {
                	?>
                     <img id="ownerLoginIcon" src="assets/icons/checked_truck-48.png" >Truck Owner Loggin</h3>
                	<?php
                }elseif (getcwd()=='C:\xampp\htdocs\OnlineTrucks\config') {
                	?>
                     <img id="ownerLoginIcon" src="../assets/icons/checked_truck-48.png" >Truck Owner Loggin</h3>
                	<?php
                }

                ?>
		  </div>
		  <div class="panel-body">
		  <!--===================================================================================
		  form content
		  ======================================================================================-->
		<form role="form" action=" 
		<?php 
         if (getcwd()=='C:\xampp\htdocs\OnlineTrucks\config') {
         	?>logOwnerIn.php <?php
         }elseif (getcwd()=='C:\xampp\htdocs\OnlineTrucks') {
         	 ?>config/logOwnerIn.php<?php        	
         }elseif (getcwd()=='C:\xampp\htdocs\OnlineTrucks\modules\owner') {
         	?>config/logOwnerIn.php<?php
         }

		?>
         " id="ownerLogginForm" method="post">
		  <div class="form-group">
		  
			<label for="Options">Login As</label>
				<select class="form-control" id="option" required>
				  <option value="">------ SELECT HERE -------</option>
				  <option value="Owner">Car Owner</option>
				  <option value="Driver">Driver</option>
				</select>
		  </div>

		  <div class="form-group">
		  
			<label for="Email">Email address</label>
			<input type="email" name="Email1" class="form-control" id="Email" placeholder="Enter email" >
		  </div>
		  
		  <div class="form-group">
			<label for="Password">Password</label>
			<input type="password" class="form-control" id="Password" name="password1" placeholder="Password" >
		  </div>
		  
		  <div class="checkbox">
			<label>
			  <input type="checkbox"> Remember me
			</label>
		  </div>
		  
		  
		  <button type="button" id="LoginOwner" class="btn btn-default" id="log">Log In</button>
		</form>
		<!--===========================================================================================-->
		  </div>
		</div>

		<div class="alert alert-info">If you are not a registred truck owner <a href="modules/owner/register.php"><strong id="regOwner">Click here </strong></a>to register.</div>
		
</div><!--END-COLUMN-->		
</div>
</div>
</div>
</body>



<script type="text/javascript">
	$(document).ready(function(){
		$('#LoginOwner').click(function(){
           var Email = $('#Email').val();
           var Password = $('#Password').val();
           var Option = $('#option').val();

           if (Email=='' || Password==''|| Option=='') {
               $('#response').html('<div class="alert alert-danger" role="alert">Fill In All the Required Fields</div>');
           }else{
           	 $.post('config/logOwnerIn.php', {email:Email, password:Password, option:Option}, function(data){
           	 	$('#response').html(data);
           	 });
           }
		})
	})
</script>
</html>