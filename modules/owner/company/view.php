<?php
@include('../../../config/connect.php');
@include('../../../config/session.php');
@include('../../../config/queryOwner.php');  ?>

<div class="panel panel-default">
  <div class="panel-body">
<h4>Company Profile</h4>
<?php
$Owner_session = $_SESSION['adminUsername'];
$queryCompany = "SELECT * FROM `truck_company` WHERE `Owner`='$Owner_session'";
$Company_result = mysqli_query($dbc, $queryCompany);

while ($Company=mysqli_fetch_array($Company_result)) {
	$company_name = $Company['Name'];
	$company_address = $Company['Address'];
}

?>


<ul class="list-group">
  <li class="list-group-item"><i class="glyphicon glyphicon-home"></i> <?=$company_name; ?> Ltd</li>
  <li class="list-group-item"><i class="glyphicon glyphicon-map-marker"></i> <?=$company_address; ?></li>

</ul>

  </div>
</div>