<?php
if (getcwd()=='C:\xampp\htdocs\OnlineTrucks\modules\owner\drivers') {
include('../../../config/connect.php');
include('../../../config/session.php');
include('../../../config/queryOwner.php');  
}?>
<div class="panel panel-default">
  <div class="panel-heading"><center>Register a new Company</center></div>
  <div class="panel-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Company Name:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="company_name" placeholder="Add Company Name">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Address:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="address" placeholder="Add Company's driver">
                </div>
              </div>
             
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> By registering this Company, you are Abiding by terms and condition of TrailSited.
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="button" id="add_company_btn" class="btn btn-default">Register Now!!</button>

                  <div id="company_status"></div>
                </div>
                
              </div>
              
            </form>
  </div>
</div>

<script>
    $("#add_company_btn").click(function(){
      var company_name1 = $("#company_name ").val();
      var address1 = $("#address").val();


      if(company_name1 ==''|| address1==''){
         $("#company_status").html('<div class="alert alert-danger" role="alert">Fill in all empty fields!</div>');
      }else{
        $.post("company/config/add_company_config.php",{company_name:company_name1, address:address1}, function(data){
            $("#company_status").html(data);
        });
      }

    })
</script>

