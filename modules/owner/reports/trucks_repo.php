<table class="table">
<thead>
	<th>#</th>
	<th>Truck Name:</th>
	<th>Plate</th>
	<th>Company</th>
	<th>Location</th>
	<th>Capacity</th>
	<th>Cargo Category</th>
	<th>Action</th>
</thead>
<?php
@include('../../../config/connect.php');
@include('../../../config/session.php');
@include('../../../config/queryOwner.php');
$a = 0;
 $query_truck = "SELECT * FROM `trucks` WHERE `Owner`='$ownerIdNo'";
$truck_result = mysqli_query($dbc, $query_truck);

 	while($truck = mysqli_fetch_array($truck_result))
	{ 
   $tId = $truck['Id'];
	$tName = $truck['truckName'];
	$tNumber = $truck['truckNumber'];
	$company=$truck['company'];
	$area = $truck['areaOfOperation'];
	$capacity = $truck['cargoCapacity'];
	$category = $truck['cargoCategory'];
	$image = $truck['imagePath'];
$a++;


	?>
<tr>
  <td><?=$a; ?></td>
  <td><?=$tName; ?></td>	
  <td><?=$tNumber; ?></td>	
  <td><?=$company; ?></td>	
  <td><?=$area; ?></td>
  <td><?=$capacity; ?></td>
  <td><?=$category; ?></td>	
  <td><button id="delete<?=$tId; ?>" class="btn btn-danger btn-sm" type="submit"><span class="glyphicon glyphicon-trash" ></span> Delete Truck</button></td>	

</tr>

<script type="text/javascript">
	$("#delete<?=$tId; ?>").click(function() {
		var id1 = "<?=$tId; ?>";
		if (id1=='') {alert("No truck selected");
	}else{
		$.post("trucks/config/delete_truck.php", {id:id1}, function(data){
			alert(data);
			$('#viewTruck').load('reports/trucks_repo.php');
		});
	}
	})
</script>
<?php
}
?>
</table>