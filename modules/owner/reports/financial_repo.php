<?php
@include('../../../config/connect.php');
@include('../../../config/session.php');
@include('../../../config/queryOwner.php');
?>
<div class="page-header">
  <h4>Filter Report Monthly</h4>
	<form class="form-inline" role="form" action="sales_report.php" method="post">
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputEmail2">Select a month</label>
		<select id="month" class="form-control" placeholder="Select a month" name="month">
		 <option value="">----- Select another month (viewing only) ------</option>
		 <option name="month" value="All">All Months</option>
         <option name="month" value="1">January</option>
         <option name="month" value="2">February</option>
         <option name="month" value="3">March</option>
         <option name="month" value="4">April</option>
         <option name="month" value="5">May</option>
         <option name="month" value="6">June</option>
         <option name="month" value="7">July</option>
         <option name="month" value="8">August</option>
         <option name="month" value="9">September</option>
         <option name="month" value="10">Octomber</option>
         <option name="month" value="11">November</option>
         <option name="month" value="12">December</option>
				</select>									    									    
	  </div>
	  <div class="form-group">
	    <label class="sr-only" >Select Category</label>
		<select id="category" class="form-control" name="category">
		 <option value="">----- Select Kind Of Request ------</option>
         <option name="category" value="All">All Requests</option>
         <option name="category" value="requested">Pending Requests</option>
         <option name="category" value="Accepted">Accepted Requests</option>
         <option name="category" value="Completed">Completed Requests</option>
         <option name="category" value="Others">Others</option>
				</select>									    									    
	  </div>
	  <div class="form-group">
	    <label class="sr-only" >Select Truck</label>
		<select id="truck" class="form-control" name="category">
		 <option value="">----- Select A Truck ------</option>
         <option  value="All">All Trucks</option>
                      <?php 
                        $tquery = "SELECT * FROM `trucks` WHERE `Owner`='$ownerIdNo' ";
                        $tr = mysqli_query($dbc, $tquery);
                          while($trucks = mysqli_fetch_array($tr)){
                             $plate = $trucks['truckNumber'];   
                         ?>
                        <option value="<?= $plate ?>" class="option"><?= $plate ?></option>
                         <?php } ?>
		</select>									    									    
	  </div>

	  <button type="button" class="btn btn-default" id="filer_report">Submit</button>
	</form> 
	<div id="report_status"></div> 
</div>


<div id="Report">
<table class="table">
<thead>
	<th>#</th>
	<th>Date:</th>
	<th>Cargo:</th>
	<th>Truck</th>
	<th>Pick-Up Address</th>
	<th>Job Status</th>
	<th>Job Cost</th>
</thead>
<?php

$a = 0;
 $query_jobs = "SELECT * FROM `jobrequest` WHERE `status`='Accepted' OR `status`='Completed' AND `truckNumber` IN (SELECT `truckNumber`FROM `trucks` WHERE `Owner`='$ownerIdNo') ";;
$job_result = mysqli_query($dbc, $query_jobs);

while($job = mysqli_fetch_array($job_result))
	{ 
    $jId = $job['Id'];
	$jcargo = $job['cargo'];
	$jtruckNumber = $job['truckNumber'];
	$jpickUpAddress=$job['pickUpAddress'];
	$jpickUpDate = $job['pickUpDate'];
	$jdropOffAddress = $job['dropOffAddress'];
	$jexpectedTime = $job['expectedTime'];
	$jstatus = $job['status'];
    $customer = $job['customerId'];
    $cost = $job['cost'];
    $a++;
?>
<tr>
	<td><?=$a; ?></td>
	<td><?=$jpickUpDate; ?></td>
	<td><?=$jcargo; ?></td>
	<td><?=$jtruckNumber; ?></td>
	<td><?=$jpickUpAddress; ?></td>
	<td><?=$jstatus; ?></td>
	<td>$ <?=$cost; ?></td>
</tr>

<?php
}

$sum = "SELECT SUM(cost) FROM `jobrequest` WHERE `status`='Accepted' AND `truckNumber` IN(SELECT `truckNumber`FROM `trucks` WHERE `Owner`='$ownerIdNo')";

	 $result_sum = mysqli_query($dbc, $sum);
	    while ($summ = mysqli_fetch_array($result_sum)){
		$total = $summ['SUM(cost)'];
	?>
<tr class="danger">
	<td></td>
	<td><h4>Total Trip Cost</h4></td>
	<td></td>
	<td></td>
	<td></td>
	<td>All Trucks</td>
	<td><strong>$ <?php echo $total; ?></strong></td>
	
</tr>
<?php
}	
?>
</table>	
</div>        

<script type="text/javascript">
	$("#filer_report").click(function(){		
      var month1 = $("#month").val();
      var category1 = $("#category").val();
      var truck1 = $("#truck").val();
      if (month1=='' || category1==''||truck1=='' ) {
         $("#report_status").html("Select A month");
      }else{
         $.post("reports/config/finance_report.php",{month:month1, category:category1, truck:truck1},function(data){
         	$("#Report").html(data);
         	$("#report_status").html("");
             $(".table").DataTable({
                    dom: 'rBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
             });           	
         })
      }
	})
</script>