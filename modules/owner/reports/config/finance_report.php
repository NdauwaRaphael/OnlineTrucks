<table class="table">
<thead>
	<th>#</th>
	<th>Date:</th>
	<th>Cargo:</th>
	<th>Truck</th>
	<th>Pick-Up Address</th>
	<th>Job Status</th>
	<th>Job Cost</th>
</thead>
<?php

@include('../../../../config/connect.php');
@include('../../../../config/session.php');
@include('../../../../config/queryOwner.php');

if (isset($_POST['month']) && !empty($_POST['month']) ){
$category= $_POST['category'];
$month = $_POST['month'];
$year = date("Y");
$truck = $_POST['truck'];
if ($month=='All') {
	$mFilter = "MONTH(pickUpDate) IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12) AND YEAR(pickUpDate)=$year";
}else{
$mFilter = "MONTH(pickUpDate)=$month AND YEAR(pickUpDate)=$year";	
}

if($truck =='All'){
 $tFilter = "`truckNumber` IN (SELECT `truckNumber`FROM `trucks` WHERE `Owner`='$ownerIdNo')";
}else{
 $tFilter="`truckNumber`= '$truck'";
}

if ($category=='Others') {
 $query_jobs = "SELECT * FROM `jobrequest` WHERE $mFilter  AND  $tFilter AND `status` !='Accepted' AND `status` !='requested' AND `status` !='Completed'  ";   
}elseif ($category=='All') {
 $query_jobs = "SELECT * FROM `jobrequest` WHERE  $mFilter AND  $tFilter ";
}else{
 $query_jobs = "SELECT * FROM `jobrequest` WHERE `status`='$category' AND $mFilter AND $tFilter ";
}


$a = 0;
$job_result = mysqli_query($dbc, $query_jobs);

while($job = mysqli_fetch_array($job_result))
	{ 
    $jId = $job['Id'];
	$jcargo = $job['cargo'];
	$jtruckNumber = $job['truckNumber'];
	$jpickUpAddress=$job['pickUpAddress'];
	$jpickUpDate = $job['pickUpDate'];
	$jdropOffAddress = $job['dropOffAddress'];
	$jexpectedTime = $job['expectedTime'];
	$jstatus = $job['status'];
    $customer = $job['customerId'];
    $cost = $job['cost'];
    $a++;
?>
<tr>
	<td><?=$a; ?></td>
	<td><?=$jpickUpDate; ?></td>
	<td><?=$jcargo; ?></td>
	<td><?=$jtruckNumber; ?></td>
	<td><?=$jpickUpAddress; ?></td>
	<td><?=$jstatus; ?></td>
	<td>$ <?=$cost; ?></td>
</tr>

<?php
}
if ($category=='Others') {
$sum = "SELECT SUM(cost) FROM `jobrequest` WHERE  $mFilter AND `truckNumber` IN(SELECT `truckNumber`FROM `trucks` WHERE `Owner`='$ownerIdNo') AND `status` !='Accepted' AND `status` !='requested' AND `status` !='Completed' AND  $tFilter";	
}elseif ($category=='All') {
$sum = "SELECT SUM(cost) FROM `jobrequest` WHERE $mFilter AND `truckNumber` IN(SELECT `truckNumber`FROM `trucks` WHERE `Owner`='$ownerIdNo') AND  $tFilter";
}else{
$sum = "SELECT SUM(cost) FROM `jobrequest` WHERE  $mFilter AND `truckNumber` IN(SELECT `truckNumber`FROM `trucks` WHERE `Owner`='$ownerIdNo') AND `status`='$category' AND  $tFilter";	
}


	 $result_sum = mysqli_query($dbc, $sum);
	    while ($summ = mysqli_fetch_array($result_sum)){
		$total = $summ['SUM(cost)'];
	?>
<tr class="danger">
	<td></td>
	<td><h4>Total Trip Costs</h4></td>
	<td></td>
	<td></td>
	<td></td>
	<td>All Trucks</td>
	<td><strong>$ <?php echo $total; ?></strong></td>
	
</tr>
<?php
}	
}
?>
</table>