<?php
include('../../config/connect.php');
include('../../config/session.php');
include('../../config/query.php');

?>
<div class="row">
  <div class="container">

    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title"><strong>TrailedSited</strong> <?php echo $fname." ".$lname; ?> User Profile</h3>
      </div>

		  <div class="panel-body alert alert-warning">
                <div class="row">
                      <h3>PERSONAL DETAILS</h3>
                  <div class="col-sm-2 col-lg-4">
                      <img src="assets/icons/gab.png" class="img-circle success">
                      <button type="button" class="btn btn-default">Upload a profile photo</button> 
                   </div>

                  <div class="col-sm-2 col-lg-8">
                     <h4><?php echo "$fname $lname";?></h4>
                  </div>

                </div><!--end row-->
                <div class="clearme"> </div>

                <div class="row">
                  <table class="table table-hover">
                    <thead>
                       <th>#</th>
                       <th>Details</th>
                       <th>Modify</th>
                    </thead>
                    <tbody>
                        <tr>
                          <td>First Name</td>
                          <td><?php echo "$fname";?></td>
                          <td><a href="#" class="pop" data-toggle="popover" data-placement="top" data-original-title="Dear <?php echo "$fname"; ?> !" data-content="You are currently not allowed to make changes on this field"><span class="glyphicon glyphicon-pencil"></span> edit</a>
                          </td>
                        </tr>

                        <tr>
                          <td>Last Name</td>
                          <td><?php echo "$lname";?></td>
                          <td><span class="glyphicon glyphicon-pencil"></span> edit</td>
                        </tr>

                        <tr>
                          <td>National Id</td>
                          <td><?php echo "$id_no";?></td>
                          <td><span class="glyphicon glyphicon-pencil"></span> edit</td>                          
                        </tr>
                        <tr>
                          <td>Email Address</td>
                          <td><?php echo "$Email";?></td>
                          <td><span class="glyphicon glyphicon-pencil"></span> edit</td>
                        </tr>

                      </tbody>

                  </table>
                  <button type="button" class="btn btn-warning">Deactivate your account !</button>

                </div><!--end 2nd row-->
		  </div>
		</div>  

  </div>
</div>