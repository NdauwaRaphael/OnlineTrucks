<div class="row">
   <div class="alert alert-success col-lg-12"> 
    <div class="container">
      <h3>If you are not Registered, <big><code>Register Now</big></code> to access our services.</h3>
    </div>
      
   </div>
 </div>


</div>

<div class="row">
<div class="container">
  <div class="row myrow">
    <H2><img src="assets/icons/add_user-48.png" alt="Hallo!!" class="img-circle">JOIN US NOW!!</H2>
  </div>
      <div class="row">
       <div class="col-lg-2"></div>
       <div class="col-lg-10">
        <div id="saveStatus"></div>
      </div>
      </div>
<form class="form-horizontal" id="registerMe">
  
    <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">First Name</label>
    <div class="col-lg-10">
      <input type="text" class="form-control" id="inputFname" placeholder="First Name">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Last Name</label>
    <div class="col-lg-10">
      <input type="email" class="form-control" id="inputLname" placeholder="Last Name">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Gender</label>
    <div class="col-lg-10">
        <select id="gender" class="form-control">
        <option value="">-------------------------------SELECT GENDER----------------------------------------</option>
          <option value="Female">Female</option>
          <option value="Male">Male</option>
          <option value="Other">Other</option>
        </select>
    </div>
  </div>  

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Id Number</label>
    <div class="col-lg-10">
      <input type="email" class="form-control" id="inputIdno" placeholder="National Identification Number">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Mobile Phone</label>
    <div class="col-lg-10">
      <input type="email" class="form-control" id="inputMobile" placeholder="Mobile Phone Number">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Email</label>
    <div class="col-lg-10">
      <input type="email" class="form-control" id="inputEmail" placeholder="Email" onchange="email_validate(this.value);">
      <p id="emailstatus"></p>
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Residence</label>
    <div class="col-lg-10">
      <input type="email" class="form-control" id="inputResidence" placeholder="Residence">
    </div>
  </div>  

  <div class="form-group">
    <label for="inputPassword1" class="col-lg-2 control-label">Password</label>
    <div class="col-lg-10">
      <input type="password" class="form-control" id="inputPassword1" placeholder="Password">
    </div>
  </div>

  <div class="form-group">
  <p id="message"></p>
    <label for="inputPassword1" class="col-lg-2 control-label">Repeat Password</label>
    <div class="col-lg-10">
      <input type="password" class="form-control" id="inputPassword2" placeholder="Confirm Password" onkeyup="checkPass(); return false;">
    </div>
  </div>
   </tr>   

  <div class="form-group">
    <div class="col-lg-offset-2 col-lg-10">
      <button id="regUserBtn" class="btn btn-success btn-lg">Sign in</button>
      Already a Member <a href="modules/membership/login.php"><strong><code>Click Here</code></strong></a> to <a href="modules/membership/login.php"><strong>Login</strong></a>
    </div>
  </div>

 </form>    
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
$('#regUserBtn').click(function(){
     var Fname = $('#inputFname').val();
     var Lname = $('#inputLname').val();
     var gender = $('#gender').val();
     var Idno = $('#inputIdno').val(); 
     var Mobile = $('#inputMobile').val(); 
     var Email = $('#inputEmail').val(); 
     var Residence = $('#inputResidence').val();
     var Ipassword = $('#inputPassword1').val();
     var Cpassword = $('#inputPassword2').val();

     if (Fname=='' || Lname=='' || gender==''|| Idno=='' || Mobile=='' || Email=='' || Residence=='' ||
      Ipassword=='') 
       {
          alert("Error!! Fill In All The Required Fields");
       }else {

      if (Ipassword == Cpassword) {
      $.post("config/regMe.php", {Fname1:Fname, Lname1:Lname, gender1:gender, Idno1:Idno, Mobile1:Mobile, Email1:Email, Residence1:Residence, Ipassword1:Ipassword }, function(data){
                  $('#saveStatus').html(data);
                  $('#registerMe')[0].reset();

             } )
     }else{
      alert('Incorrect password combination, Your password do not match');
     }

     }        

   }); 


/*Validating*/
function checkPass()
{
       
    
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('inputPassword1');
    var pass2 = document.getElementById('inputPassword2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('message');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match";

        $('#regUserBtn').prop('disabled', false);
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.

        $('#regUserBtn').prop('disabled', true);
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
} 

// validates text only
function Validate(txt) {
    txt.value = txt.value.replace(/[^a-zA-Z-'\n\r.]+/g, '');
}
// validate email
function email_validate(email)
{
var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
   var status = document.getElementById("emailstatus");
    if(regMail.test(email) == false)
    {    
    document.getElementById("emailstatus").innerHTML    = "<span class='warning'>Email address is not valid yet.</span>";
        status.style.color = "#f44336";
        $('#regUserBtn').prop('disabled', true);
        $('#regOwner').prop('disabled', true);
    }
    else
    {
    document.getElementById("emailstatus").innerHTML	= "<span class='valid'>Thanks, you have entered a valid Email address!</span>";	
        status.style.color = "#a5d6a7";
        $('#regUserBtn').prop('disabled', false);
        $('#regOwner').prop('disabled', false);
    }
}   

})
</script>