<?php
include('config/connect.php');
include('config/session.php');
include('config/query.php');
include('config/queryOwner.php');
include('config/check_active.php');
if (!loggedin()) {
   header("location: index.php");
}else{
 if (!user_active()) {
  header("location: error/error.php");
} 
}



?>

<html>
<head>
<title>Online Trucks Reservation</title>
<?php include('config/css.php'); ?>
<style type="text/css">
  #sort{
  padding: 10px;

}
.event a, .ui-datepicker .event span {
    background-color: #ffffff !important;
    color: #ff0000 !important;
}

.event a {
    background-color: #ff0000 !important;
    color: #000000 !important;
}

  #hereContent {
  margin-top: 0px auto;
  background-image:url(assets/images/background.jpg);
  background-attachment: fixed;
  color: #ccc;
  border-bottom: 2px solid #DC143C;

}

  #homeBg {
  margin-top: 0px auto;
  padding: 10px;
  background-image:url(assets/images/homeBig.jpg);
  background-attachment: fixed;
  color: #ccc;
  border-bottom: 2px solid #DC143C;
 }
#homeBg::after {
  content: "";

  opacity: 0.4;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: -1;

}



</style>
</head>



<body>
	
	<div class="nav " id="navi">
  <div id="navBg">
       <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle " data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                  <div class="truckImageLogo">
                    <image src="assets/icons/truck.png"  >Online Trucks
                  </div>
              </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li ><a href="home.php" ><span class="glyphicon glyphicon-home"></span> Home</a></li>
            	<li><a id="requests"><span class="glyphicon glyphicon-comment"></span> Requests</a></li>
            	<li><a href="#services"><span class="glyphicon glyphicon-bell"></span> Notifications</a></li>
            	<li><a href="#faqs"><span class="glyphicon glyphicon-question-sign"></span> FAQs</a></li>
             </ul>
             <ul class="nav navbar-nav navbar-right">
             	<li class="active">
                 <a href="#acct"><button class="btn btn-success" id="btnProf"><span class="glyphicon glyphicon-user"> </span> <?php echo $fname." ".$lname; ?></button></a>
             	</li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-wrench"></span> Options <b class="caret"></b></a>
        <ul class="dropdown-menu" id="navBg">
          <li><a href="#">Profile</a></li>
          <li><a href="#">Settings</a></li>
          <li><a href="#">Messages</a></li>
          <li><a href="#">Requests</a></li>
          <li><a href="config/logMeOut.php"><span class="glyphicon glyphicon-off"></span> logout</a></li>
        </ul>
      </li>

             </ul>                   	
	</div>
</div>
</div>
</div>
<!--end navigation area-->

<div id="hereContent">
  <div class="container">
  <div id="homeBg">
  <div id="hom"></div>
  </div>

  </div>
</div>

<footer>
  <div class="container">
    <div class="row">
        <div class="col-sm-2">
            <h6>copyright: &copy; 2016 Online Trucks App Develop. All Rights Reserved</h6>
        </div><!--end col-small-2-->

        <div class="col-sm-4">
              <h6>About Us</h6>
              <p>Know more about us</p>
              <p><input type="text" id="datepicker"></p>
        </div><!--end col-small-4-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
             <p><span class="glyphicon glyphicon-heart"> </span> Managed and Designed by Us</p>
        </div>

    </div><!--end row-->

  </div><!--end footer container-->

</footer>



</body>
<?php include "config/js.php"; ?>
<script type="text/javascript">
 $(document).ready(function(){
  $('#hom').load('modules/Trucks/view.php');

var navpos =$('#navi').offset();
$(window).scroll(function(){
  console.log($(window).scrollTop());

  if ($(window).scrollTop()>navpos.top) {
    $('#navi').addClass('navbar-fixed-top');
  }

  if ($(window).scrollTop()<151) {
    $('#navi').removeClass('navbar-fixed-top');
  }
})
 })
</script>

</html>