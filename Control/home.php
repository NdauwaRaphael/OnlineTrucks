<?php
include('../config/connect.php');
include('config/session.php');

if (!loggedin_Admin()) {
  header("location: login.php");
}
?>

<html>
<head>
<title>Online Trucks Reservation</title>
<?php include('../config/css.php'); ?>
<style type="text/css">
  #sort{
  padding: 10px;

}
.event a, .ui-datepicker .event span {
    background-color: #ffffff !important;
    color: #ff0000 !important;
}

.event a {
    background-color: #ff0000 !important;
    color: #000000 !important;
}

  #hereContent {
  margin-top: 0px auto;
  background-image:url(assets/images/background.jpg);
  background-attachment: fixed;
  color: #ccc;
  border-bottom: 2px solid #DC143C;

}

  #homeBg {
  margin-top: 0px auto;
  padding: 10px;
  background-image:url(assets/images/homeBig.jpg);
  background-attachment: fixed;
  color: #ccc;
  border-bottom: 2px solid #DC143C;
 }
#homeBg::after {
  content: "";

  opacity: 0.4;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: -1;

}



</style>
<?php include "../config/js.php"; ?>
</head>



<body>
	
	<div class="nav " id="navi">
  <div id="navBg">
       <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle " data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                  <div class="truckImageLogo">
                    <image src="../assets/icons/truck.png"  >Online Trucks
                  </div>
              </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li ><a href="home.php" ><span class="glyphicon glyphicon-home"></span> Home</a></li>
             </ul>
             <ul class="nav navbar-nav navbar-right">
             	<li class="active">
                 <a href="#acct"></a>
             	</li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-wrench"></span> Options <b class="caret"></b></a>
        <ul class="dropdown-menu" id="navBg">
            <li><a href="logout.php">Logout</a></li>
        </ul>
      </li>

             </ul>                   	
	</div>
</div>
</div>
</div>
<!--end navigation area-->

<div id="hereContent">
  <div class="container">

<div >
  <div class="col-xs-5">
  <h4>Truck Owners</h4>
  <hr>
<table class="table table-hover">
<thead class="success">
  <th>First Name</th>
  <th>Last Name</th>
  <th>Email</th>
  <th>Phone </th>
  <th>Action</th>
</thead>

<?php
  $select_owners = "SELECT * FROM `owner membership`";
  $result_owner = mysqli_query($dbc, $select_owners);
  while ($owner = mysqli_fetch_array($result_owner)) {
     $firstname = $owner['Firstname'];
     $lastname = $owner['Lastname'];
     $email = $owner['Email'];
     $idno = $owner['IdNo'];
     $tel = $owner['tel'];
     $control = $owner['admin_control'];
     $id = $owner['Id'];
?>
 <tr>
    <td><?=$firstname; ?></td>
    <td><?= $lastname; ?></td>
    <td><?=$email; ?></td>
    <td><?=$tel; ?></td>
    <td><button class="btn btn-default" id="action<?=$idno;?>" type="submit">
          <?php if ($control=='Active') {
            ?>Deactivate<?php
          }else{
            ?>Activate<?php
            } ?></button></td>
 </tr>

<script type="text/javascript">
  $("#action<?=$idno;?>").click(function(){
    var id1 ="<?=$id;?>";
    var status1 = "<?=$control;?>";
    if (id1==''||status1=='') {
        alert("No User Selected");
    }else{
       $.post("config/owner_control.php",{id:id1, status:status1}, function(data){
        alert(data);
        window.location.href="home.php"
       })
    }
  })
</script>  

<?php

  }
?>    
</table> 
  </div>
<div class="col-xs-1">h</div>
  <div class="col-xs-6">
    
        <h4>Truck Customers</h4>
        <hr>
      <table class="table table-hover">
      <thead class="success">
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Id No</th>
        <th>Action</th>
      </thead>

      <?php
        $select_users = "SELECT * FROM `membership`";
        $result_user = mysqli_query($dbc, $select_users);
        while ($owner = mysqli_fetch_array($result_user)) {
           $firstname = $owner['Firstname'];
           $lastname = $owner['Lastname'];
           $email = $owner['Email'];
           $idno = $owner['IdNo'];
           $control = $owner['admin_control'];
           $id = $owner['Id'];
      ?>
       <tr>
          <td><?=$firstname; ?></td>
          <td><?= $lastname; ?></td>
          <td><?=$email; ?></td>
          <td><?=$idno; ?></td>
          <td><button class="btn btn-default" id="action<?=$id;?>" type="submit">
          <?php if ($control=='Active') {
            ?>Deactivate<?php
          }else{
            ?>Activate<?php
            } ?></button></td>
       </tr>

<script type="text/javascript">
  $("#action<?=$id;?>").click(function(){
    var id1 ="<?=$id;?>";
    var status1 = "<?=$control;?>";

    if (id1==''||status1=='') {
        alert("No User Selected");
    }else{
       $.post("config/control.php",{id:id1, status:status1}, function(data){
        alert(data);
        window.location.href="home.php"
       })
    }
  })
</script>       
      <?php

        }
      ?>    
      </table>     
  </div>
</div>
  </div>
</div>

<footer>
  <div class="container">
    <div class="row">
        <div class="col-sm-2">
            <h6>copyright: &copy; 2016 Online Trucks App Develop. All Rights Reserved</h6>
        </div><!--end col-small-2-->

        <div class="col-sm-4">
              <h6>About Us</h6>
              <p>Know more about us</p>
              <p><input type="text" id="datepicker"></p>
        </div><!--end col-small-4-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
             <p><span class="glyphicon glyphicon-heart"> </span> Managed and Designed by Us</p>
        </div>

    </div><!--end row-->

  </div><!--end footer container-->

</footer>



</body>

<script type="text/javascript">

</script>

</html>