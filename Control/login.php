<?php
include('config/session.php');

if (loggedin_Admin()) {
  header("location: home.php");
}
?>

<html>
<head>
<title>Online Trucks Reservation</title>
<?php include('../config/css.php'); ?>
<style type="text/css">
  #sort{
  padding: 10px;

}
.event a, .ui-datepicker .event span {
    background-color: #ffffff !important;
    color: #ff0000 !important;
}

.event a {
    background-color: #ff0000 !important;
    color: #000000 !important;
}

  #hereContent {
  margin-top: 0px auto;
  background-image:url(assets/images/background.jpg);
  background-attachment: fixed;
  color: #ccc;


}

  #homeBg {
  margin-top: 0px auto;
  padding: 10px;
  background-image:url(assets/images/homeBig.jpg);
  background-attachment: fixed;
  color: #ccc;

 }
#homeBg::after {
  content: "";

  opacity: 0.4;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: -1;

}



</style>
</head>



<body>
  
  <div class="nav " id="navi">
  <div id="navBg">
       <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle " data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                  <div class="truckImageLogo">
                    <image src="../assets/icons/truck.png"  >Online Trucks
                  </div>
              </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li ><a href="../home.php" ><span class="glyphicon glyphicon-home"></span> Home</a></li>
             </ul>
             <ul class="nav navbar-nav navbar-right">
              <li class="active">
                 <a href="#acct"></a>
              </li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-wrench"></span> Options <b class="caret"></b></a>
        <ul class="dropdown-menu" id="navBg">

        </ul>
      </li>

             </ul>                    
  </div>
</div>
</div>
</div>
<!--end navigation area-->

<div id="hereContent">
  <div class="container">
  <div id="homeBg">
  <div id="hom">
    
 <div class="panel panel-default">
  <div class="panel-body">
      <form action="login.php" method="post">
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" id="email" class="form-control" placeholder="Email">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" id="password" class="form-control" placeholder="Password">
        </div>

        <button type="button" id="login" class="btn btn-default">Submit</button>
      </form>
      <div id="status"></div>

 
  </div>
</div>
   
  </div>
  </div>

  </div>
</div>





</body>
<?php include "../config/js.php"; ?>
 <script type="text/javascript">
    $("#login").click(function() {
      var email1 = $("#email").val();
      var password1 = $("#password").val();

      if (email1==''||password1=='') {
         $("#status").html("fill in empty data");
      }else{
        $.post("config/login.php",{email:email1, password:password1}, function(data){
          $("#status").html(data);
        })
      }
    })

  </script>  

</html>