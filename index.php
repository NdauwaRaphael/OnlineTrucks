<?php
include('config/connect.php');
include('config/session.php');

if (loggedin()) {
  header("location: home.php");
}
if (ADMIN()) {
  header("location: modules/owner/home.php");
}
?>

<html>
<head>
<title>Welcome Karibu Online Trucks Reservation</title>
<?php include('config/css.php'); ?>
<style type="text/css">
  .mybtn{
    margin: 10px auto;
    background-color: #e57373;
    color: #fff;
    font-weight: narrow;
    font-size: 1.2em;
    border: 1px solid #1b5e20;
    border-radius: 0.5em;
  }

  .mybtn:hover {
    background-color: #f44336;
    color: #4caf50;
  }
 .area {
  text-align: center;
  color: #e57373;
  animation: blur 1s ease-out infinite;
  text-shadow: 0px 0px 5px #fff, 0px 0px 7px #fff;
}

#leaseimg {
  width: 80%; 
}
</style>
</head>



<body>
	<div id="navBg">
	<div class="nav">
       <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                  <div class="truckImageLogo">
                    <image src="assets/icons/truck.png"  >Online Trucks
                  </div>
              </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                
            	<li><a href="#about">About Us</a></li>
            	<li><a href="#services">Services</a></li>
            	<li><a href="#faqs">FAQs</a></li>
            	<li><a href="#contacts">Contacts</a></li>
             </ul>
             <ul class="nav navbar-nav navbar-right">
             	<li class="active">
                 <a href="#acct"><button id="btnReg" class="btn btn-success"><span class="glyphicon glyphicon-user"></span> Join Now!!</button></a>
             	</li>
             </ul>                   	
	</div>
</div>
</div>
</div>
<!--end navigation area-->
<div id="hom">
	<div class=" start">
       <div class="container">
           <h3>Hallo world</h3>
           <p><big>TrailedSited</big> is a service company for tracing your favourite cargo transporting trucks and companies in proximaty to any area a client is located by using our unique algorithm.</p>
         <div class="hom">
           <div class="col-md-5">
              <p>Only here you can be able to select the suitable truck to transport any product with a proximity to your present area; Select capacity; Conviniency to the type of cargo; Schedule a pickup time; a Pickup location and then specifie the dirrection without any physical contact with these truck owners</p>
              <p>Place your <big>Order</big> and choose yourself the desired vehicle, and company to transport your products.</p>
              <p>Pay only after your reservation Order have been approved.</p>
           </div>
           <div class="col-md-2"> </div>
           <div class="col-md-5">
            <H3><u>Login Now to HIRE a truck</u></H3>

               <!--lOGIN FORM HERE -->
                  <form class="form-horizontal" action="config/logMeIn.php" method="post" id="logmeform" role="form">
                    <div class="form-group">
                      <label for="inputEmail1" class="col-lg-2 control-label">Email</label>
                      <div class="col-lg-10">
                        <input type="email" name="Email1" class="form-control" id="thisEmail1" placeholder="Email">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword1" class="col-lg-2 control-label">Password</label>
                      <div class="col-lg-10">
                        <input type="password" name="password1" class="form-control" id="thisPassword1" placeholder="Password">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-lg-offset-2 col-lg-10">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox"> Remember me
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" class="btn btn-default">Log in</button>
                      </div>
                    </div>
                  </form>
               <!--END LOGIN FORM-->
           </div>
         </div> <!--end row-->
       </div> <!--end home start container-->
	</div>
	<!--end home start area-->

</div>

<div id="lease">
  <div class="container">
      <div class="col-md-4">
      <img src="assets/images/front.png" id="leaseimg">
      </div>
      <div class="col-md-4">
                <div class="area">
          <h3> Do you have a truck you wanna Lease <img src="assets/icons/pipedT.png"> ??</h3>
           <span><button class="btn mybtn" id="ownerLoginBtn"><span class="glyphicon glyphicon-globe"></span> Lease Now!!</button></span>
           </div>
        
      </div>
      <div class="col-md-4">
        <img src="assets/images/front1.jpg" id="leaseimg">
      </div>
  </div>
</div>

<footer>
  <div class="container">
    <div class="row">
        <div class="col-sm-2">
            <h6>copyright: &copy; 2016 Online Trucks App Develop. All Rights Reserved</h6>
        </div><!--end col-small-2-->

        <div class="col-sm-4">
              <h6>About Us</h6>
              <p>A Technology company dedicated to give unlimited access to truck services by bringing together Truck owners/companies and their respective customer in a platform where they can exchange services. <strong>Are you a Truck Owner, <strong><button class="btn btn-link" >login here</button></strong></strong></p>
        </div><!--end col-small-4-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
          <h6>Navigation</h6>
          <ul class="unstyled">
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">LinkedIn</a></li>
              <li><a href="#">Google plus</a></li>
              <li><a href="#">E-News</a></li>
          </ul>
        </div><!--end col-small-2-->

        <div class="col-sm-2">
             <p><span class="glyphicon glyphicon-heart"> </span> Managed and Designed by Us</p>
        </div>

    </div><!--end row-->

  </div><!--end footer container-->

</footer>

</body>
<?php include "config/js.php"; ?>
</html>