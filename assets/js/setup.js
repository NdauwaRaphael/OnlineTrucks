$(document).ready(function(){
/*Jobs here -owner*/
  $('#jNew').click(function(){
   $('#viewTruck').load('jobs/jobs.php');
  });

  $('#jPView').click(function(){
     $('#viewTruck').load('jobs/pendingJobs.php',function(){

             $(".table").DataTable({
                    dom: 'rBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
             });            
        });
  });  

  $('#JcView').click(function(){
     $('#viewTruck').load('jobs/completed.php', function(){

             $(".table").DataTable({
                    dom: 'rBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
             });            
        });
  });   


$("#requests").click(function(){
$('#hom').load('modules/feedbacks/requests.php', function(){

             $(".table").DataTable({
                    dom: 'rBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
             });            
        });
});

/*====Add Truck -Owner*/
  $('#tAdd').click(function(){
    $('#viewTruck').load('trucks/add.php');
  });

/*====View Truck -Owner*/
  $('#tView').click(function(){
    $('#viewTruck').load('trucks/ownerView.php');
  });  

/*===Truck Drivers==*/
  $("#dAdd").click(function(){
    
    $('#viewTruck').load('drivers/hire.php');
  });

  $("#dView").click(function(){
    
    $('#viewTruck').load('drivers/view.php', function(){

             $(".table").DataTable({
                    dom: 'rBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
             });            
        });
  });

  /*=========Company=============*/  
    $('#cAdd').click(function(){
    $('#viewTruck').load('company/create.php');
  });

    $('#cView').click(function(){
    $('#viewTruck').load('company/view.php');
  });  

  /*===========Reports============*/  
    $('#truck_repo').click(function(){
    $('#viewTruck').load('reports/trucks_repo.php', function(){

             $(".table").DataTable({
                    dom: 'rBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
             });            
        });
  }); 

      $('#job_repo').click(function(){
    $('#viewTruck').load('reports/trips_repo.php', function(){

             $(".table").DataTable({
                    dom: 'rBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
             });            
        });
  }); 
     $('#finance_repo').click(function(){
     $('#viewTruck').load('reports/financial_repo.php', function(){

             $(".table").DataTable({
                    dom: 'rBfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
             });            
        });
  });   
/*============*/

 /*===Index Page Scripts*/
   $('#btnReg').click(function () {
      $('#hom').load('modules/membership/register.php');
   });

   $('#ownerLoginBtn').click(function(){

     $('#hom').load('modules/owner/login.php');

   });

   $('#log').click(function(){
       $('#ownerLogginForm').load('config/logOwnerIn.php');
   });


  /*===Registering new Owner===*/

$('#regOwnerBtn').click(function(){
     var Fname = $('#inputFname').val();
     var Lname = $('#inputLname').val();
     var gender = $('#gender').val();
     var Idno = $('#inputIdno').val(); 
     var Mobile = $('#inputMobile').val(); 
     var Email = $('#inputEmail').val(); 
     var Ipassword = $('#inputPassword1').val();
     var Cpassword = $('#inputPassword2').val(); 

     if (Ipassword == Cpassword) {
         
         if (Fname=='' || Lname=='' || gender==''|| Idno=='' || Mobile=='' || Email=='' || Ipassword=='') 
         {
           alert('FILL IN ALL THE FIELDS REQUIRED!!');
         }else{
             $.post("../../config/regOwner.php", {Fname1:Fname, Lname1:Lname, gender1:gender, Idno1:Idno, Mobile1:Mobile, Email1:Email, Ipassword1:Ipassword }, function(data){
                  $('#saveStatus').html(data);
                  $('#registerMe')[0].reset();

             } )

         }

     }else{
      alert('Incorrect password combination, Your password do not match');

     }

   });

/*===============================Register User===============================*/

   /*===User Homepage====*/
   $('#btnProf').click(function () {
    alert('ouch!!');
      $('#hom').load('modules/Template/profile.php');
   });

 /*============Home pagination========*/
   


});

/*Lef nav*/
/*==========================================================
      Script for home left nav
====================================================*/
        function openNav() {
          document.getElementById('mySidenav').style.width = '300px';
        }

        function closeNav() {
          document.getElementById('mySidenav').style.width = '0';
        }  


     // var divClone = $("#share").clone();
      if ($('body').width() >= 1200) { 
       // $("#shared").html($("#share"));
       $("#mySidenav").attr('id', 'sidedbar');

       }else{
         $("#sidedbar").attr('id', 'mySidenav');
       }

/*======On resizing window*/
/*Use of $('body').width() instead $(window).width() because when rezing using maximizing or restore 
button the script will not get the width thus it confused*/

         $(window).resize(function(){
              if ($('body').width() >= 1200) { 
               $("#mySidenav").attr('id', 'sidedbar');
             }else{
               $("#sidedbar").attr('id', 'mySidenav');
             }
         });

